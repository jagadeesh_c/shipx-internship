# The sum of the squares of the first ten natural numbers is,
# 1^2 + 2^2 + ... + 10^2 = 385
# The square of the sum of the first ten natural numbers is,
# (1 + 2 + ... + 10)^2 = 55^2 = 3025
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
# 3025 - 385 = 2640
# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.



#"sum of the squares" of natural number upto a given number
def sum_of_square(num)

    res = 0                 # to store all the squares of numbers
    1.upto(num) do |i|
        res += i ** 2 
    end
    res 
end 


# "square of sum" of natural number upto a given number
def square_of_sum(num)
   
    res = 0
    1.upto(num) do |i|
        res += i
    end 
    res ** 2                #square of sum of numbers
end


# to calculate difference between the "sum of the squares" of natural numbers and the "square of the sum"  
def calculate(num)

    square_of_sum(num) - sum_of_square(num)
end


puts calculate(100)