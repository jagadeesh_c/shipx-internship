# A palindromic number reads the same both ways. 
# The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.



# returns true for a palindrome string or number
def is_palindrome(number)

    number = number.to_s                                            # to convert number to string
    for i in 0..(number.length)/2 - 1                               #iterate through each char of the string

        return false if number[i] != number[number.length - i - 1]  # check the first and the last char. then, next char and so on
    end
    true
end


# to find largest palindrome from the product of 3-digit numbers
def largest_palindrome

    # check if the product of two numbers is palindrome for all combination from 999 to 100
    999.downto(100) do |i|  

        999.downto(100) do |j|

            if is_palindrome(i * j)                                 # true if the number is palindrome
                puts "#{i} x #{j}"
                return
            end
        end
    end
end

# calling "largest_palindrome" function
largest_palindrome