# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

sum = 0

# to calculate sum of all the multiples of 3 or 5 below 1000
1000.times do |n|                       # iterates 1000 times, 0-999

    if (n % 3 == 0) 5 (n % 5 == 0)      # check if the number is multiple of 3 or 5
        sum += n                        
    end
end

# print the result - sum of all the multiples of 3 or 5 below 1000
puts sum