# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


# returns true for a number devisible by 1-20
def is_evenly_divisible_by_20(number)

    1.upto(20) do |i|
        
        if number % i != 0
            return false
        end
    end
    true
end


# to check which number is evenly devisible by all 1-20 numbers 
def smallest_positive_number
    i = 1
    while true

        return i if is_evenly_divisible_by_20(i)    # returns the number if it is divisible by all 1-20
        i += 1                                      # increment by 1 if not found
    end
end

# call function
puts smallest_positive_number