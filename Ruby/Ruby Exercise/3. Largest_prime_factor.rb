# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?



# returns all the factors of a numnber
def get_factors(number)

    factors = []
    for i in 1..(Math.sqrt(number)) do
        
        if number % i == 0
            
            factors.append(i)
            factors.append( number / i )
        end
    end
    factors
end


# returns true for a prime number
def is_prime(number)
    
    return (get_factors(number)).length == 2
end


# get all the factors of a number
NUMBER = 600851475143
factors =  get_factors(NUMBER)

# to get the largest prime factor
largest_prime_factor = 0

factors.each do |factor|

    if is_prime(factor) and factor > largest_prime_factor
        largest_prime_factor = factor
    end
end

puts "Largest prime factor of #{NUMBER} is : #{largest_prime_factor}"

