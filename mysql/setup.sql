DROP DATABASE IF EXISTS shipxDB;
create database shipxDB;

use shipxDB;

create table departments(
	department_id int not null,
	department_name varchar(255),
	`group` varchar(255),
    primary key(department_id)
);


create table employees (
	employee_id int not null,
	`name` varchar(255),
	city varchar(255),
	state varchar(255),
	age int,
	department_id int not null,
	primary key(employee_id),
    foreign key(department_id) references departments(department_id)
);

insert into departments
values (1, 'Development', 'Engineering'),
(2, 'QA', 'Engineering'),
(3, 'Consulting', 'Services'),
(4, 'Support', 'Services'),
(5, 'Sales', 'Sales');

insert into employees
values (1,	'Ashish',	'Bengaluru',	'Karnataka',	25,	1),
(2,	'Bindu',	'Chennai',	'Tamil Nadu',	37,	2),
(3,	'Chethan',	'Hyderabad',	'Telangana',	39,	1),
(4,	'Danny',	'Bengaluru',	'Karnataka',	26,	1),
(5,	'Eva',	'Chennai',	'Tamil Nadu',	27,	2),
(6,	'Fatima',	'Hyderabad',	'Telangana',	25,	3),
(7,	'Gaurav',	'Mumbai', 'Maharashtra',	29,	2),
(8,	'HImesh',	'New Delhi',	'Delhi',	31,	1),
(9,	'Indu',	'Bengaluru',	'Karnataka',	26,	2),
(10,	'Jayan',	'Chennai',	'Tamil Nadu',	28,	1),
(11,	'Ketan',	'Mumbai',	'Maharashtra',	29,	3),
(12,	'Lisa',	'Mysuru',	'Karnataka',	31,	4),
(13,	'Manu',	'Mysuru',	'Karnataka',	27,	1),
(14,	'Nandini',	'Chennai',	'Tamil Nadu',	30,	3),
(15,	'Osman',	'New Delhi',	'Delhi',	32,	4),
(16,	'Peter',	'Bengaluru',	'Karnataka',	35,	1);
